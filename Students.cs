﻿using System;
using System.Collections.Generic;

namespace quest_student_app
{
    public partial class Students
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Course { get; set; }
        public int? Batch { get; set; }
        public int? Semester { get; set; }
        public int? RollNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public long? UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public virtual Users User { get; set; }
    }
}
