﻿using System;
using System.Collections.Generic;

namespace quest_student_app
{
    public partial class Users
    {
        public Users()
        {
            Students = new HashSet<Students>();
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PasswordDigest { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string RememberDigest { get; set; }
        public string ActivationDigest { get; set; }
        public bool? Activation { get; set; }
        public DateTime? ActivatedAt { get; set; }

        public virtual ICollection<Students> Students { get; set; }
    }
}
